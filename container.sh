#!/bin/bash

rm -rf dist

yarn build

docker rm -f api-vendas

docker images prune

docker build -t jmcosta/api-vendas .

docker run --name api-vendas -d -t -p 3333:3333 \
-e REDIS_HOST=34.121.174.119 \
-e REDIS_PORT=6379 \
-e DB_TYPE="postgres" \
-e DB_HOST="34.72.125.238" \
-e DB_PORT="5432" \
-e DB_USER="dev" \
-e DB_PASSWORD="dev" \
-e DB_DATABASE="api-vendas-dev" \
jmcosta/api-vendas

docker ps -a
