
import AppError from "@shared/errors/AppError";
import { getCustomRepository } from "typeorm";
import Order from "../typeorm/entities/Order";
import { OrdersRepository } from "../typeorm/repositories/OrdersRepository";

interface IPaginateOrder {
  from: number;
  to: number;
  per_page: number;
  total: number;
  current_page: number;
  prev_page: number | null;
  next_page: number | null;
  data: Order[];
}

class ListOrderService {
  public async execute(): Promise<IPaginateOrder> {

    const ordersRepository = getCustomRepository(OrdersRepository);

    const order = await ordersRepository.createQueryBuilder().paginate();

    if(!order){
      throw new AppError('Orders not found.');
    }

    return order as IPaginateOrder;

  }
}

export default ListOrderService;
