import 'reflect-metadata';
import 'dotenv/config';
import 'express-async-errors';
import '@shared/typeorm';
import express, { NextFunction, Request, Response } from 'express';
import cors from 'cors';
import { errors } from 'celebrate';
import routes from './routes';
import AppError from '@shared/errors/AppError';
import uploadConfig from '@config/upload';
import { pagination } from 'typeorm-pagination';
import rateLimiter from '@shared/http/middlewares/rateLimiter';

const app = express();

app.use(pagination);
app.use(cors());
app.use(express.json());

// ratelimiter tem que estar anter de qualquer rota
app.use(rateLimiter);

app.use('/files', express.static(uploadConfig.directory));
app.use(routes);
app.use(errors());
app.use(
  (error: Error, request: Request, response: Response, next: NextFunction) => {
    if (error instanceof AppError) {
      return response.status(error.statusCode).json({
        status: 'error',
        message: error.message,
      });
    }

    console.log(error);

    return response.status(500).json({
      status: 'error',
      message: 'Internal server error',
    });
  }
);

const port = process.env.APP_PORT || 3333;

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
