FROM node:alpine

WORKDIR /usr/src/app

COPY . ./

RUN npm install

RUN ls -a

EXPOSE 3333

CMD ["npm", "start"]
