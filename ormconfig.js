module.exports = {
  "type": process.env.DB_TYPE,
  "host": process.env.DB_HOST,
  "port": process.env.DB_PORT,
  "username": process.env.DB_USER,
  "password": process.env.DB_PASSWORD,
  "database": process.env.DB_DATABASE,
  "entities": ["./modules/**/typeorm/entities/*.js",
               "./src/modules/**/typeorm/entities/*.ts"],
  "migrations": ["./shared/typeorm/migrations/*.js",
                 "./src/shared/typeorm/migrations/*.ts"],
  "cli": {
    "migrationsDir": "./shared/typeorm/migrations" || "./src/shared/typeorm/migrations"
  }
}
